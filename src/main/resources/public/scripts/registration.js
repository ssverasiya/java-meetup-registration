$(document).ready(function() { 
	$("#participant-form").submit(function(e) {
		
		$('body').loadingModal({
			text:'Saving...',
			animation: 'wanderingCubes'
		});
		
		var participant = {
			"fullName": $("#fullname").val(),
		    "address": $("#address").val(),
			"email": $("#email").val(),
		    "password": $("#password").val(),
			"phone": $("#phone").val()
		};
		
		$.ajax({
			type: "POST",
			url: "/rest/registration",
			data: JSON.stringify(participant),
			contentType: 'application/json',
			cache: false,
			success: function(participant) {
				$('body').loadingModal('hide');
				$('#participant-form').trigger("reset");
				
				$('#saved-name').text(participant.fullName);
				$('#saved-address').text(participant.address);
				$('#saved-email').text(participant.email);
				$('#saved-phone').text(participant.phone);
				
				$('#participantModal').modal('show');
			},
			error: function (xhr, status, error) {
				$('body').loadingModal('hide');
				$.growl.warning({ title: "Exists!", message: xhr.responseText });
	        }
		});
	});
});	