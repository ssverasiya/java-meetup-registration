package com.k15t.pat.registration;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.k15t.pat.model.Participant;
import com.k15t.pat.repositories.ParticipantRepository;


@Path("/registration")
@Component
public class RegistrationResource {

	@Autowired
	private ParticipantRepository participantRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
    
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
    public Response registerParticipant(Participant participant) {
		String email = participant.getEmail();
		if(participantRepository.findByEmail(email) != null) {
			return Response.status(Response.Status.CONFLICT).entity(participant.getEmail() + " has already been registered with us!").type("text/plain").build();
		}
		
		participant.setPassword(passwordEncoder.encode(participant.getPassword()));
		Participant savedParticipant = participantRepository.save(participant);
        return Response
        		.status(Response.Status.CREATED)
        		.entity(savedParticipant)
        		.build();
    }
	
	@GET
	@Path("/participants")
	@Produces(MediaType.APPLICATION_JSON)
    public Response getAllParticipants() {
        return Response
        		.status(Response.Status.OK)
        		.entity(participantRepository.findAll())
        		.build();
    }

}
