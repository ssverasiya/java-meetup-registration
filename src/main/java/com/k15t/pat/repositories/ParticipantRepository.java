package com.k15t.pat.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.k15t.pat.model.Participant;

@Repository
public interface ParticipantRepository extends CrudRepository<Participant, Integer> {
	
	Participant findByEmail(String email);

}
