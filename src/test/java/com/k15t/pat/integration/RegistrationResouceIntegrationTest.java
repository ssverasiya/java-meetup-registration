package com.k15t.pat.integration;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.k15t.pat.ApplicationBootstrap;
import com.k15t.pat.model.Participant;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(ApplicationBootstrap.class)
@WebIntegrationTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class RegistrationResouceIntegrationTest {
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Value("${local.server.hostname}")
	private String hostname;
	
	@Value("${local.server.port}")
	private int port;
	
	private Participant participant;
	
	private ObjectMapper objectMapper = new ObjectMapper();
	
	private RestTemplate restTemplate = new TestRestTemplate();
	
	private HttpHeaders headers = new HttpHeaders();
	
	@Before
	public void before() {
		headers.setContentType(MediaType.APPLICATION_JSON);
		setupParticipant();
	}

	/**
	 * GIVEN: a new participant
	 * WHEN: the REST service is called to save the participant
	 * THEN: the participant is successfully saved
	 * 
	 * @throws Exception
	 */
    @Test
    public void should_successfully_save_a_new_participant() throws Exception {
		
    	HttpEntity<String> request = new HttpEntity<>(objectMapper.writeValueAsString(participant), headers);

    	ResponseEntity<String> response = restTemplate
    	  .exchange(hostname + ":" + port + "/rest/registration", HttpMethod.POST, request, String.class);

    	Participant savedParticipant = objectMapper.readValue(response.getBody(), Participant.class);
    	assertEquals(HttpStatus.CREATED, response.getStatusCode());
    	assertEquals(savedParticipant.getEmail(), participant.getEmail());
    	assertEquals(passwordEncoder.matches(participant.getPassword(), savedParticipant.getPassword()), true);
    }
    
    /**
	 * GIVEN: saved participants
	 * WHEN: the REST service is called to retrieve all the registered participants
	 * THEN: the list of participants are retrieved
	 * 
	 * @throws Exception
	 */
    @Test
    public void should_retrieve_all_the_registered_participants() throws Exception {

    	HttpEntity<String> request = new HttpEntity<>(objectMapper.writeValueAsString(participant), headers);
    	restTemplate.exchange(hostname + ":" + port + "/rest/registration", HttpMethod.POST, request, String.class);
    	
    	request = new HttpEntity<>(headers);
    	ResponseEntity<String> response = restTemplate.
    											exchange("http://localhost:8080/rest/registration/participants", HttpMethod.GET, request, String.class);

    	List<Participant> participants = objectMapper.readValue(response.getBody(), new TypeReference<List<Participant>>(){});

    	assertEquals(HttpStatus.OK, response.getStatusCode());
    	assertEquals(participants.size(), 1);
    }
    
    /**
	 * GIVEN: an already saved participant
	 * WHEN: the REST service is called to save a new participant with an already taken email address
	 * THEN: 409 Conflict is thrown
	 * 
	 * @throws Exception
	 */
    @Test
    public void show_throw_conflict_if_email_address_already_registered() throws Exception {
    	
    	HttpEntity<String> request = new HttpEntity<>(objectMapper.writeValueAsString(participant), headers);
    	
    	ResponseEntity<String> response = restTemplate
    	    	  .exchange(hostname + ":" + port + "/rest/registration", HttpMethod.POST, request, String.class);

    	assertEquals(HttpStatus.CREATED, response.getStatusCode());
    	
    	response = restTemplate
    	    	  .exchange("http://localhost:8080/rest/registration", HttpMethod.POST, request, String.class);

    	assertEquals(HttpStatus.CONFLICT, response.getStatusCode());
    	assertEquals(response.getBody(), participant.getEmail()  + " has already been registered with us!");
    }
    
    private Participant setupParticipant() {
    	
    	return participant =  Participant.builder()
										.fullName("Sameer Siraj")
										.email("sameer.siraj@gmail.com")
										.address("new address")
										.password("abc123!")
										.phone("132465465").build();
    }
}
