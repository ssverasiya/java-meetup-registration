package com.k15t.pat.repository;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.k15t.pat.ApplicationBootstrap;
import com.k15t.pat.model.Participant;
import com.k15t.pat.repositories.ParticipantRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(ApplicationBootstrap.class)
@EnableJpaRepositories(basePackageClasses = ParticipantRepository.class)
@EntityScan(basePackageClasses = ParticipantRepository.class)
public class ParticipantRepositoryTest {

    @Autowired
    private ParticipantRepository participantRepository;
    
    private Participant participant;

    @Test
    public void should_save_participant() {

    	participantRepository.save(setupParticipant());

        Participant participant = participantRepository.findOne(1);
        assertThat(participant, notNullValue());
    }
    
    @Test
    public void should_find_participant_by_email() {

    	participantRepository.save(setupParticipant());

        Participant participant = participantRepository.findByEmail(this.participant.getEmail());
        assertThat(participant, notNullValue());
        assertEquals(participant.getEmail(), "sameer.siraj@gmail.com");
    }
    
    private Participant setupParticipant() {
    	
    	return participant =  Participant.builder()
										.fullName("Sameer Siraj")
										.email("sameer.siraj@gmail.com")
										.address("new address")
										.password("abc123!")
										.phone("132465465").build();
    }
}
