## Java Meetup Registration Project
Foremost, I'd like to thank K15t for placing their trust on me to help them in this important project. I hope to live up to the expectations and also to collaborate more so that we continue building new softwares together.

## What is it about?
This project now includes an important feature on allowing people from different parts of the world to register for an exciting Java meetup scheduled to be organised this year in K15t office in Stuttgart, Germany. The feature consists of a microservice and a frontend consuming the service to facilitate the registration process.
 
## The Technology Stack (What additionally is added)
As it is known that the application is built on top of Spring boot having Jersey for engineering the REST resources and for the user interfaces, Velocity is used as a templating engine along with JQuery/Bootstrap. Additionally, the following technologies have been added to add value to the project:

* MySQL database has been added as a data store for persisting and managing the registered participants.
* A schema change management tool "Liquibase" is used to keep track and deploy schema changes.
* Docker is used for bootstrapping the infrastructure services and also permitting to package and deploy the application with its dependencies using containers.
* A CI/CD pipeline with Bitbucket is added, enabling automation from integration and testing phases to delivery and deployment.
* Project [Lombok](https://projectlombok.org/) has been added to the project to avoid repetitive and boilerplate code.
* For the sake of better documentation of our REST services, "Enunicate" is introduced which provides full HTML documentation of the REST services. More can be read [here](https://github.com/stoicflame/enunciate/wiki)

## How to start up the Application
In order to first fetch the application's repository, clone it from the following URL 

```
#!bash
git clone https://bitbucket.org/ssverasiya/java-meetup-registration.git
```

Once it is successfully cloned, please execute the following command at first to run the build and create the executables.

```
#!bash
cd PATH_TO_THE_PROJECT
mvn clean install -DskipTests
```
### Using Docker
The application can easily be deployed using docker compose files. All you need is to execute the following command to start up the services: (registration-service, mysqldb). The file "docker-compose.yml" is used in this case.

```
#!bash
docker-compose -f docker-compose.yml up -d
```

Please note that docker builds the image for the "registration-service" by coping the executable jar residing in the "./target" directory of the project. This can be improved in the future and the pushed image can be fetched from a Nexus repository directly.

### Using an IDE
To start the application using an IDE like Eclipse or IntelliJ, please update the file "application.properties" of the project and update the hostname to "localhost" in the connection string of the property like the following,

```
spring.datasource.url = jdbc:mysql://localhost:3306/pat-registration?createDatabaseIfNotExist=true&useSSL=false
```

Please re-build the project to have changes in affect using the command

```
#!bash
mvn clean install -DskipTests
```

Use the file "docker-compose-local.yml" to bootstrap the infrastructure services first. To do so, execute the following command.

```
#!bash
docker-compose -f docker-compose-local.yml up -d
```

The above command will start services: (mysqldb). Now start the application by running it as a java application from the class "ApplicationBootstrap" and you're good to go. 

**Register yourself by testing both the ways above for the meetup ;)**

## Tests
The application houses two types of tests for now which are the integration and repository tests. An in-memory database H2 is used to facilitate the testing. Explicitly run the tests by the following command ensuring that the all the tests are green.

```
#!bash
mvn test
```

## Enunciate Documentation ##

To see what REST services exist in our application and what they offer, execute the following maven goal,

```
#!bash
mvn enunciate:docs
```

The API docs will be created in the directory (./target/docs/apidocs). As an example, Please open the file "resource_RegistrationResource.html" to know how well the services residing in the RegistrationResource are documented.

## CI/CD pipeline

The CI/CD pipeline is configured using bitbucket. The pipeline results can be seen on this [URL](https://bitbucket.org/ssverasiya/java-meetup-registration/addon/pipelines/home)


That's about it with regards to the documentation and the updates on the project. I await for a constructive feedback and look forward for more collaborations like these. Thank you :)